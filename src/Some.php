<?php


class Some
{
    public function selfFilename()
    {
        return pathinfo(
            __FILE__,
            PATHINFO_BASENAME
        )['basename']; // Тут допустил ранее ошибку и, например, не протестил и потом например отформатировал
    }

    public function selfFolder()
    {
        return __DIR__;
    }

    public function selfClassName()
    {
        return __CLASS__;
    }
}
