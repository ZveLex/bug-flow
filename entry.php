<?php

include './src/Some.php';

$some = new Some();

echo implode(
        PHP_EOL,
        [
            $some->selfFolder(),
            $some->selfFilename(),
            $some->selfClassName(),
        ]
    ) . PHP_EOL;
